File Handling
	-Open black_cards.txt file
	-Open used_phrases.txt file
Tweeting a phrase
	-Tweet a random phrase from black_cards.txt
	-Write it in used_phrases.txt
	-Delete it from black_cards.txt
Tweeting the most favorited
	-"Stream" the tweet replies
	-Only keep the one with most favorites
	-Tweet the round winning user
