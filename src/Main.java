import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;

public class Main {

	private static List<String> blackCardsList;
	private static List<String> usedCardsList;

	public static void main(String[] args) throws InterruptedException {

		blackCardsList = new LinkedList<String>();
		usedCardsList = new LinkedList<String>();

		try {

			// Tries to open the text files if successful writes in console
			File blackCards = new File("C:/Users/JDuarte/workspace/CardsAgainstHumanityTwitterBot/black_cards.txt");
			File usedPhrases = new File("C:/Users/JDuarte/workspace/CardsAgainstHumanityTwitterBot/used_phrases.txt");
			System.out.println("Files opened successfully.");

			// Tries to load the last state of the application if successful
			// writes in console
			loadPhrases(blackCards, blackCardsList);
			loadPhrases(usedPhrases, usedCardsList);
			System.out.println("Phrases loaded successfully.");

		} catch (NullPointerException | FileNotFoundException e) {
			System.err.println("File not found at the given path.");
		}

		// Accesses the Twitter API using the twitter4j.properties file
		Twitter twitter = TwitterFactory.getSingleton();
		
		//Runs until stopped by user input
		while(true){
			try {
				tweetMessage(twitter);
				Thread.sleep(1000 * 10);
			} catch (TwitterException e) {
				System.err.println("Couldn't connect to the Twitter API.");
			}
		}
	}

	private static void loadPhrases(File textFile, List<String> phrasesList) throws FileNotFoundException {
		Scanner phraseLoader = new Scanner(new FileReader(textFile));
		while (phraseLoader.hasNextLine()) {
			phrasesList.add(phraseLoader.nextLine());
		}
		phraseLoader.close();
	}

	private static void tweetMessage(Twitter twitter) throws TwitterException {
		Random randomPhrasePicker = new Random();
		int pickedPhraseIndex = randomPhrasePicker.nextInt(blackCardsList.size());
		String pickedPhrase = blackCardsList.get(pickedPhraseIndex);
		twitter.updateStatus(pickedPhrase);
		usedCardsList.add(pickedPhrase);
		blackCardsList.remove(pickedPhraseIndex);
		System.out.println("Sucessfully tweeted:\n-" + pickedPhrase);
	}

}
